package com.ip.testingexamples.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class Offer {
    private String offerId;
    private boolean offerAccepted;
    private int productId;

    public Offer(String offerId, int productId) {
        this.offerId = offerId;
        this.productId = productId;
    }

    public String getOfferId() {
        return offerId;
    }

    public boolean isOfferAccepted() {
        return offerAccepted;
    }

    public void setOfferAccepted(boolean offerAccepted) {
        this.offerAccepted = offerAccepted;
    }

    public int getProductId() {
        return productId;
    }

    public void accept() {
        setOfferAccepted(true);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Offer offer = (Offer) o;

        return new EqualsBuilder()
                .append(offerAccepted, offer.offerAccepted)
                .append(productId, offer.productId)
                .append(offerId, offer.offerId)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(offerId)
                .append(offerAccepted)
                .append(productId)
                .toHashCode();
    }

    @Override
    public String toString() {
        return "Offer{" +
                "offerId='" + offerId + '\'' +
                ", offerAccepted=" + offerAccepted +
                ", productId=" + productId +
                '}';
    }
}