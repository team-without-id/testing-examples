package com.ip.testingexamples.domain;

import org.apache.commons.lang3.Validate;

import java.util.Date;

public class Email {
    private Offer offer;
    private Date sendDate = new Date();

    public Email(Offer offer) {
        Validate.notNull(offer, "No offer - no emails");
        this.offer = offer;
    }

    public Offer getOffer() {
        return offer;
    }

    public Date getSendDate() {
        return sendDate;
    }
}