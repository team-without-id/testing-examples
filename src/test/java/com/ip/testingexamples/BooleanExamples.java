package com.ip.testingexamples;

import com.ip.testingexamples.domain.Offer;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertFalse;

public class BooleanExamples {
    private Offer offer;

    @Before
    public void before() {
        offer = new Offer("cff47c9c-29ff-4742-9535-8cd8a2dfa90e", 1);
    }

    @Test
    public void newOfferShouldNotBeAccepted() {
        // junit
        assertFalse(offer.isOfferAccepted());

        // hamcrest
        assertThat(offer.isOfferAccepted(), equalTo(false));

        // assertj
        assertThat(offer.isOfferAccepted()).isFalse();
    }

    @Test
    public void newOfferFailsJunit() {
        offer.accept();
        assertFalse(offer.isOfferAccepted());
    }

    @Test
    public void newOfferFailsHamcrest() {
        offer.accept();
        assertThat(offer.isOfferAccepted(), equalTo(false));
    }

    @Test
    public void newOfferFailsAssertJ() {
        offer.accept();
        assertThat(offer.isOfferAccepted()).isFalse();
    }
}