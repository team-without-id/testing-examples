package com.ip.testingexamples;

import com.ip.testingexamples.domain.Offer;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.data.Percentage.withPercentage;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.CombinableMatcher.both;
import static org.junit.Assert.assertTrue;

public class NumberExamples {
    private Offer offer;

    @Before
    public void before() {
        offer = new Offer("cff47c9c-29ff-4742-9535-8cd8a2dfa90e", 1);
    }

    // Negative

    @Test
    public void shouldBeNegative() {
        // junit
        assertTrue(-5 < 0);

        // hamcrest
        assertThat(-5, lessThan(0));

        // assertj
        assertThat(-5).isNegative();
    }

    @Test
    public void shouldBeNegativeFailsJunit() {
        assertTrue(5 < 0);
    }

    @Test
    public void shouldBeNegativeFailsHamcrest() {
        assertThat(5, lessThan(0));
    }

    @Test
    public void shouldBeNegativeFailsAssertJ() {
        assertThat(5).isNegative();
    }

    // Between

    @Test
    public void shouldBeBetween() {
        // junit
        assertTrue(5 > 0 && 5 < 10);

        // hamcrest
        assertThat(5, is(both(greaterThan(0)).and(lessThan(10))));

        // assertj
        assertThat(5).isBetween(0, 10);
    }

    @Test
    public void shouldBeBetweenFailsJunit() {
        assertTrue(-5 > 0 && -5 < 10);
    }

    @Test
    public void shouldBeBetweenFailsHamcrest() {
        assertThat(-5, is(both(greaterThan(0)).and(lessThan(10))));
    }

    @Test
    public void shouldBeBetweenFailsAssertJ() {
        assertThat(-5).isBetween(0, 10);
    }


    @Test
    public void otherExamples() {
        assertThat(5).inBinary().isEqualTo(0b101);
        assertThat("012").containsOnlyDigits();

        // hamcrest compares absolute difference
        assertThat((double) offer.getProductId(), closeTo(6.0, .5));
        // Expected: a numeric value within <0.5> of <6.0>
        // but: <1.0> differed by <4.5>

        // assertj compares relative difference
        assertThat(offer.getProductId()).isCloseTo(6, withPercentage(10));
        // Expecting:
        // <1>
        // to be close to:
        // <6>
        // by less than 10% but difference was 83.33333333333334%.
        // (a difference of exactly 10% being considered valid)
    }
}