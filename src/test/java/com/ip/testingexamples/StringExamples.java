package com.ip.testingexamples;

import com.ip.testingexamples.domain.Offer;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.StringStartsWith.startsWith;
import static org.junit.Assert.assertTrue;

public class StringExamples {
    private Offer offer;

    @Before
    public void before() {
        offer = new Offer("cff47c9c-29ff-4742-9535-8cd8a2dfa90e", 1);
    }

    // Equals

    @Test
    public void shouldBeEqual() {
        // junit
        assertEquals("cff47c9c-29ff-4742-9535-8cd8a2dfa90e", offer.getOfferId());

        // hamcrest
        assertThat(offer.getOfferId(), equalTo("cff47c9c-29ff-4742-9535-8cd8a2dfa90e"));

        // assertj
        assertThat(offer.getOfferId()).isEqualTo("cff47c9c-29ff-4742-9535-8cd8a2dfa90e");
    }

    @Test
    public void shouldBeEqualFailsJunit() {
        assertEquals("-cff47c9c-29ff-4742-9535-8cd8a2dfa90e", offer.getOfferId());
    }

    @Test
    public void shouldBeEqualFailsHamcrest() {
        assertThat(offer.getOfferId(), equalTo("-cff47c9c-29ff-4742-9535-8cd8a2dfa90e"));
    }

    @Test
    public void shouldBeEqualFailsAssertJ() {
        assertThat(offer.getOfferId()).isEqualTo("-cff47c9c-29ff-4742-9535-8cd8a2dfa90e");
    }

    // Contains

    @Test
    public void shouldContainSubstring() {
        // junit
        assertTrue(offer.getOfferId().contains("29ff"));

        // hamcrest
        assertThat(offer.getOfferId(), containsString("29ff"));

        // assertj
        assertThat(offer.getOfferId()).contains("29ff");
    }

    @Test
    public void shouldContainFailsJunit() {
        assertTrue(offer.getOfferId().contains("29ee"));
    }

    @Test
    public void shouldContainFailsHamcrest() {
        assertThat(offer.getOfferId(), containsString("29ee"));
    }

    @Test
    public void shouldContainFailsAssertJ() {
        assertThat(offer.getOfferId()).contains("29ee");
    }

    // Starts with

    @Test
    public void shouldStartWith() {
        // junit
        assertTrue(offer.getOfferId().startsWith("c"));

        // hamcrest
        assertThat(offer.getOfferId(), startsWith("c"));

        // assertj
        assertThat(offer.getOfferId()).startsWith("c");
    }

    @Test
    public void shouldStartWithFailsJunit() {
        assertTrue(offer.getOfferId().startsWith("0"));
    }

    @Test
    public void shouldStartWithFailsHamcrest() {
        assertThat(offer.getOfferId(), startsWith("0"));
    }

    @Test
    public void shouldStartWithFailsAssertJ() {
        assertThat(offer.getOfferId()).startsWith("0");
    }

    @Test
    public void otherAssertJExamples() {
        assertThat(offer.getOfferId()).containsPattern("29ff|9535");
        assertThat(offer.getOfferId()).matches("c.*");
        assertThat("5").isBetween("0", "9");
        assertThat(offer.getOfferId()).containsIgnoringCase("29FF");
        assertThat(offer.getOfferId()).doesNotContainAnyWhitespaces();
        assertThat(offer.getOfferId()).doesNotContain("29ee");
        assertThat(offer.getOfferId()).isEqualToIgnoringWhitespace(offer.getOfferId());
        assertThat(offer.getOfferId()).isEqualToNormalizingNewlines(offer.getOfferId());
    }
}