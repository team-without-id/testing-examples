## Testing examples

This project contains examples of commonly used test assertions implemented
 in JUnit, Hamcrest and AssertJ. 
Examples contain both successful and failing tests to also display the difference 
in how assertion libraries present failures to compare how easy it is
 to find out what exactly went wrong from just looking at the execution log.

Let's take a look at ```NumberExamples```. 
The first test here verifies number under test is negative.
Because result will be successful anyway there is no need in three separate tests:
```
    @Test
    public void shouldBeNegative() {
        // junit
        assertTrue(-5 < 0);

        // hamcrest
        assertThat(-5, lessThan(0));

        // assertj
        assertThat(-5).isNegative();
    }
```

Successful test is followed by three failing examples which should be separated to see how assertion errors are differ:
```
    @Test
    public void shouldBeNegativeFailsJunit() {
        assertTrue(5 < 0);
    }

    @Test
    public void shouldBeNegativeFailsHamcrest() {
        assertThat(5, lessThan(0));
    }

    @Test
    public void shouldBeNegativeFailsAssertJ() {
        assertThat(5).isNegative();
    }
```

Because IDE can run tests in random order when executed all together it is not possible to find heads or tails.
The best way to run these examples is to select 4 related tests and execute only this group.